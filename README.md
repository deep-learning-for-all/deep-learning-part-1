# IEEE@UCI Deep Learning Workshop
## Introduction to Neural Networs and PyTorch Worshop

This workshop introduces the math of basic neural networks as well as introducing the use of the deep learning framework PyTorch for experimenting with neural networks and deep learning.

This repository contains the following:
* [Slideshow](https://gitlab.com/deep-learning-part-1/-/blob/master/Deep%20Learning%20and%20PyTorch.pptx)
* [Lecture Notes](https://gitlab.com/deep-learning-part-1/-/blob/master/Pytorch_lecture_notes.md)
* [PyTorch Installation Guide](https://gitlab.com/deep-learning-part-1/-/blob/master/PyTorch_download_guide.md)
* [PyTorch Example Template](https://gitlab.com/deep-learning-part-1/-/blob/master/mnist_final.py)
* [PyTorch Example Final Code](https://gitlab.com/deep-learning-part-1/-/blob/master/mnist_final.py)

The PyTorch Installation Guide should be done first in order to have PyTorch (and Python) to complete the rest of the workshop.

This workshop leads attendees through an example CNN in PyTorch using the MNIST Handwritten Digits Dataset. An empty template is provided as mnist_template.py; a filled out version of the template is provided as mnist_final.py.

This workshop also quickly explains how one would deploy a trained PyTorch model in another environment, such as a RaspberryPi, by pickling the weights and loading them into the model at another destination.