import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms

class MNIST_Net( torch.nn.Module ):
    
    def __init__( self ):
        super( MNIST_Net, self ).__init__()
        self.conv = nn.Conv2d( 1, 16, 3 )
        self.pooling = nn.MaxPool2d( 3, stride=1 )
        self.flatten = nn.Flatten()
        self.linear1 = nn.Linear( 16*24*24, 100 )
        self.linear2 = nn.Linear( 100, 10 )
        self.softmax = nn.LogSoftmax( dim=1 )

    def forward( self, x ):
        conv_layer = F.relu( self.conv( x ) )
        pooled_layer = self.pooling( conv_layer )
        flattened = self.flatten( pooled_layer )
        hidden_relu = F.relu( self.linear1( flattened ) )
        prediction = self.linear2( hidden_relu )
        prediction = self.softmax( prediction )
        return prediction

batch_size, in_dim, hidden_dim, out_dim = 50, 28*28, 100, 10

transform = transforms.Compose( [ transforms.ToTensor(), transforms.Normalize( (0.1307,), (0.3081,) ) ] )

# download datasets if not already there
training_set = datasets.MNIST( "./data", train=True, download=True, transform=transform )
test_set = datasets.MNIST( "./data", train=False, download=True, transform=transform )

# get iterable data loader
train_loader = torch.utils.data.DataLoader( training_set, batch_size=batch_size )
test_loader = torch.utils.data.DataLoader( test_set, batch_size=batch_size )

device = torch.device( "cpu" )

model = MNIST_Net()
alpha = 1E-5

criterion = nn.NLLLoss()
optimizer = torch.optim.Adam( model.parameters(), lr=alpha )

def train( model, device, train_loader, criterion, optimizer ):
    model.train()
    for batch_id, ( data, target ) in enumerate( train_loader ):
        # data = data.view( data.shape[0], -1)
        data, target = data.to( device ), target.to( device )
        optimizer.zero_grad()
        output = model( data )
        loss = criterion( output, target )
        loss.backward()
        optimizer.step()

def test( model, device, test_loader, criterion ):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            # data = data.view( data.shape[0], -1 )
            data, target = data.to( device ), target.to( device )
            output = model( data )
            test_loss += criterion( output, target )
            prediction = output.argmax( dim=1 )
            correct += prediction.eq( target.view_as( prediction ) ).sum().item()
    test_loss /= len( test_loader.dataset )

    print( f"Epoch { epoch }: \n\tAverage Loss: { test_loss }\n\tAccuracy: { correct / len( test_loader.dataset ) }" )

for epoch in range( 10 ):
    train( model, device, train_loader, criterion, optimizer )
    test( model, device, test_loader, criterion )
torch.save( model.state_dict(), "./mnist_weight_pickle.p" )
